+++
date = '2023-10-16'
description = 'A Theatrical Research workshop, about (the myths of) romantic love.'
title = 'Workshop: "Unbreak my heart"'
[[resources]]
src = 'cover.jpg'
name = 'cover'
+++
We are very happy to announce our workshop:  "Unbreak my heart", a Theatrical Research workshop about (the myths of) romantic love.

This physical theatre workshop is a playground to exchange our take on (the myths of) romantic love in a creative way. 

We will work with creation tools like: embodiement, storytelling and writing through the body. With a critical eye, humor, and a lot of love, we will trace the patriarchal patterns that have become engraved in our bodies through these stories and give them a new perspective.

When & Where: 21/10/2023 at 18.00h. 
Espai Jove laCasa Groga (c. Ali Bei, 120, 08013, Barcelona)

Book your spot [here](https://docs.google.com/forms/d/e/1FAIpQLSeYKhviMCLnTg9HHx29caL_7hc2DpqIT-uFxWmPttggGDyaQA/viewform?usp=sharing).

