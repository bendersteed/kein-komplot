+++
date = '2024-06-14'
title = '«F.A.M.E.» in Godoff theatre magazine in Madrid, Spain!'
[[resources]]
src = 'cover.jpg'
name = 'cover'
+++

So happy that our performance F.A.M.E.(Female Artists Managing Expectations) was nominated for the
Godot Audience Award in Madrid!! You can read more [here](https://revistagodot.com/cartelera-teatro-madrid/f-a-m-a/).
