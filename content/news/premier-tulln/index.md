+++
date = '2023-08-13'
title = '«F.A.M.E.» premier in Austria, Tulln'
[[resources]]
src = 'cover.jpeg'
name = 'cover'
+++

Excited to share with you the article from NÖN magazine for our show in Tulln, Austria! You can read it [here](https://www.noen.at/tulln/premiere-von-kein-komplot-f-a-m-e-theaterperformance-in-der-kwt-380859796)! 
