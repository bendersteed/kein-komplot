+++
date = '2024-03-13'
title = '«F.A.M.E.» returns to Madrid, Spain'
[[resources]]
src = 'cover.webp'
name = 'cover'
+++

We are excited to share with you the article from el Duende for our next show in Madrid, Spain! You can read it [here](https://revistaelduende.com/plan/fama-la-actuacion-como-lucha-universal/)!
