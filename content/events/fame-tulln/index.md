+++
date = '2024-06-14'
description = ''
title = "F.A.M.E."
subtitle = "Female Artists Managing Expectations"
event_date = 2024-08-08T20:00:00+01:00
venue = "Kunstwerkstatt Tulln"
address = "Albrechtsgasse 18"
city = "Tulln, Austria"
+++
