+++
date = '2024-06-14'
description = ''
title = "F.A.M.E."
subtitle = "Female Artists Managing Expectations"
event_date = 2024-08-02T18:30:00+01:00
venue = "Kultur Sommer Wien Festival"
address = "Meischlgasse 1230"
city = "Vienna, Austria"
link = "https://kultursommer.wien/act/?id=336568-kein-komplot-lucid-dreams-theater-f-a-m-e"
link_description = "View Festival Page"
+++
