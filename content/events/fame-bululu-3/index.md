+++
date = '2024-05-18'
description = ''
title = "F.A.M.E."
subtitle = "Female Artists Managing Expectations"
event_date = 2024-05-25T20:30:00+01:00
venue = "Sala Bululú 2120"
address = "C. de Canarias, 16, Arganzuela, 28045"
city = "Madrid, Spain"
ticketing = "https://teatro.bululu2120.com/f-a-m-a-sala-bululu/"
+++
