+++
date = '2023-05-20'
description = ''
title = "F.A.M.E."
subtitle = "Female Artists Managing Expectations"
event_date = 2023-06-04T17:00:00+01:00
venue = "La casa dels contes"
address = "C/ de Ramón y Cajal, 35"
city = "Barcelona, Spain"
ticketing = "https://www.atrapalo.com/entradas/kein-komplot-teatre-gestual-_e4894119/"
+++
