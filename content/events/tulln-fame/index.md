+++
date = '2022-07-12'
description = ''
title = "F.A.M.E."
subtitle = "Female Artists Managing Expectations"
event_date = 2023-08-12T20:30:00+01:00
venue = "Kunstwerkstatt Tulln"
address = "Albrechtsgasse 18"
city = "Tulln, Austria"
link = "https://www.kunstwerkstatt.at/wp/?p=13376"
link_description = "View Festival Page"
+++
