+++
date = '2023-05-18'
description = ''
title = "F.A.M.E."
subtitle = "Female Artists Managing Expectations"
event_date = 2023-06-08T20:00:00+01:00
venue = "OFF Latin"
address = "Calle de los Mancebos, 4"
city = "Madrid, Spain"
ticketing = "https://offlatina.com/espectaculo/f-a-m-a/"
+++
