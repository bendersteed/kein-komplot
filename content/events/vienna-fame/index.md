+++
date = '2022-07-13'
description = ''
title = "F.A.M.E."
subtitle = "Female Artists Managing Expectations"
event_date = 2023-08-10T18:30:00+01:00
venue = "Kultursommer Wien"
address = " Mortarapark 1200"
city = "Vienna, Austria"
link = "https://kultursommer.wien/act/?id=33471-kein-komplot-lucid-dreams-theater-f-a-m-e-female-artists-managing-expectation"
link_description = "View Festival Page"
+++
