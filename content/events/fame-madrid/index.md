+++
date = '2023-09-06'
description = ''
title = "F.A.M.E."
subtitle = "Female Artists Managing Expectations"
event_date = 2023-09-24T18:00:00+01:00
venue = "La Encinca Teatro"
address = "C. de Ercilla 15"
city = "Madrid, Spain"
ticketing = "https://entradium.com/events/f-a-m-a"
+++
