+++
date = '2022-04-06'
description = ''
title = "Little Fly"
trailer = "https://www.youtube.com/watch?v=Hr2MTQXJjDE"
support = "With the support of Faberllull Residency, Olot Catalunya 2019"
language = "LANGUAGE: English, Spanish"
duration = "DURATION: 15'"
[[resources]]
src = '3.jpeg'
name = 'header'
+++
A brief light. A shadow. Again. A light. The shiny surface of metal stuck between two lips. Dark.  Lightning.  The diffuse silhoutte of three bodies in the space. 
The sound of a shrill bell. 
Fight begins. 

LITTLE FLY is a physical theatre performance that explores power relations through the movement of four bodies in a limited space. Seeking to visually represent the social and hierarchical inequalities that we experience in our societies.

A small fly speaks to us, in first person, whose wings have been removed. Repeat over and over again that she doesn't care...
But, what is power?  Are we born with the need to have it ? What are our repressed freedoms? And what is the line that separates the power from oppression?

The performance is a theatrical movement-piece with a non-linear dramaturgy. The creation is based on the use of four elements: the boxing ring, the animalisation of chicken, ropes that create disconnected connections, and the constant buzz of flies. 

## Trailer

{{< yt Zn_WLq96Cq0 >}}
