+++
date = '2022-04-06'
description = ''
title = "F.A.M.E."
subtitle = "(Female Artists Managing Expectations)"
trailer = "https://www.youtube.com/watch?v=Hr2MTQXJjDE"
support = "With the support of Rai Assosiació centre cultural in Barcelona Spain, Art Residency April-May 2022, ElCorralito Terassa, Spain July-August 2022 and SiFactoria Art Residency, Barcelona, 2023."
language = "LANGUAGE: English, Spanish, Catalan, German, Greek"
duration = "DURATION: 50'"
[[resources]]
src = '7.jpg'
name = 'header'
+++

 
It is a theater of creation or devising project that explores the platform format. This play tells
the epic story of a young woman from the provinces who travel to the "big city" to live her dreams
and become successful and famous actress. However, her adventure in the city will be more like an
obstacle course than a Hollywood movie and she will have to face all kinds of trials, rivals and,
above all, her own fears, in order to carry out this chimera.

Four actresses on the platform give life to this "young promise" as well as to all the characters
that enter the scene in this crazy adventure. The performers thus function as a sort of chorus that
unfolds presenting different social roles and archetypes associated with the world of show business.

In small format and under the blanket of physical comedy, this play is a critique of the ruthless
and competitive process of trying to find your place in art and a world that, despite cloaking
itself in progressiveness and avant-garde, hides an unfair and cruel treatment for its inhabitants,
especially for women.

## Trailer

{{< yt S_8DXJVjNoo >}}
For the Spanish version check [here](https://www.youtube.com/watch?v=XN5yABlV6Ak).
