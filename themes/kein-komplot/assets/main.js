const mobile_menu = document.getElementById("menu-mobile")
const open_button = document.getElementById("toggle-button")
const close_button = document.getElementById("close-mobile-menu")

open_button.addEventListener('click', () => {
    mobile_menu.setAttribute("menu-visible", "true");
})

close_button.addEventListener('click', () => {
    mobile_menu.setAttribute("menu-visible", "false")
})

document.addEventListener('mousedown', function(event) {
    var elem = event.target;
    var closest = elem.closest("#menu-mobile");
    if (closest) {

        // console.log("click inside")
    } else {

        // hide menu
        mobile_menu.setAttribute("menu-visible", "false");  
    }
})

if (document.getElementById('gallery')) {
    const lightbox = GLightbox({selector: ".glightbox" });

    const gallery_left = document.getElementById("gallery-left")
    const gallery_right = document.getElementById("gallery-right")

    const gallery = document.getElementById('gallery')
    
    gallery_left.addEventListener('click', (e) => {
        gallery.scrollBy({
            left: -300,
            behavior: "smooth"
        })
    })

    gallery_right.addEventListener('click', (e) => {
        gallery.scrollBy({
            left: 300,
            behavior: "smooth"
        })
    })
}

const projects = document.getElementById("projects-presentations");
const options = { infinite: true,
                  transition: "slide",
                  Autoplay: {
                      timeout: 4000,
                      pauseOnHover: false
                  },
                  Navigation: false,
                };

if (projects) {
    new Carousel(projects, options, { Autoplay });
}


